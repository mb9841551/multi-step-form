// const curr = 1;
const form1 = document.querySelector(".step-info-1");
const form2 = document.querySelector(".step-info-2");
const form3 = document.querySelector(".step-info-3");
const form4 = document.querySelector(".step-info-4");
const form5 = document.querySelector(".step-info-5");
const btns1 = document.querySelector(".step-1");
const btns2 = document.querySelector(".step-2");
const btns3 = document.querySelector(".step-3");
const btns4 = document.querySelector(".step-4");
const nextBtn1 = document.querySelector(".next-step-1");
const prevBtn2 = document.querySelector(".prev-step-2");
const nextBtn2 = document.querySelector(".next-step-2");
const prevBtn3 = document.querySelector(".prev-step-3");
const nextBtn3 = document.querySelector(".next-step-3");
const prevBtn4 = document.querySelector(".prev-step-4");
const nextBtn4 = document.querySelector(".confirm");
const sideBarCircle1 = document.querySelector(".steps-sidebar-1");
const sideBarCircle2 = document.querySelector(".steps-sidebar-2");
const sideBarCircle3 = document.querySelector(".steps-sidebar-3");
const sideBarCircle4 = document.querySelector(".steps-sidebar-4");
const toggle = document.getElementById("check");

let planMonthly = true;
let planYearly = false;

let x = 0;

function renderNextOrPreviousPage(
  hideCurrPage,
  showNextPage,
  currPageParentBtn,
  nextPageParentBtn,
  currCircle,
  nextCircle
) {
  hideCurrPage.style.display = "none";
  showNextPage.style.display = "flex";
  currPageParentBtn.style.display = "none";
  nextPageParentBtn.style.display = "flex";
  currCircle.classList.remove("active");
  nextCircle.classList.add("active");
}
// document.addEventListener("DOMContentLoaded", () => {

// form-1

nextBtn1.addEventListener("click", (event) => {
  const nameInput = document.getElementById("name");
  const emailInput = document.getElementById("email");
  const phoneInput = document.getElementById("phone");

  // Email validation regex
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Phone number validation regex
  const phoneRegex = /^\+?[0-9]+(?:[ -]?[0-9]+)*$/;

  if (nameInput.value == "") {
    nameInput.previousElementSibling.querySelector(".error").style.display =
      "block";
    nameInput.style.borderColor = "red";
    return;
  } else {
    nameInput.previousElementSibling.querySelector(".error").style.display =
      "none";
    nameInput.style.borderColor = "";
  }

  if (!emailRegex.test(emailInput.value)) {
    emailInput.previousElementSibling.querySelector(".error").style.display =
      "block";
    emailInput.style.borderColor = "red";
    return;
  } else {
    emailInput.previousElementSibling.querySelector(".error").style.display =
      "none";
    emailInput.style.borderColor = "";
  }
  if (!phoneRegex.test(phoneInput.value)) {
    phoneInput.previousElementSibling.querySelector(".error").style.display =
      "block";
    phoneInput.style.borderColor = "red";
    return;
  } else {
    phoneInput.previousElementSibling.querySelector(".error").style.display =
      "none";
    phoneInput.style.borderColor = "";
  }

  renderNextOrPreviousPage(
    form1,
    form2,
    btns1,
    btns2,
    sideBarCircle1,
    sideBarCircle2
  );
});

//switch between monthly and yearly

toggle.addEventListener("change", switchPlanForMonthlyAndYearly);

function switchPlanForMonthlyAndYearly(event) {
  event.preventDefault();
  const monthlySwitch = document.querySelector(".monthly-switch");
  const yearlySwitch = document.querySelector(".yearly-switch");
  const allPlanPrices = document.getElementsByClassName("plan-price");
  const allPlanPricesArray = Array.from(allPlanPrices);
  const planInfo = document.getElementsByClassName("plan-info");
  const planInfoArray = Array.from(planInfo);
  const monthPlans = ["$9/mo", "$12/mo", "$15/mo"];
  const yearPlans = ["$90/yr", "$120/yr", "$150/yr"];

  if (toggle.checked) {
    monthlySwitch.style.color = "hsl(231, 11%, 63%)";
    yearlySwitch.style.color = "hsl(213, 96%, 18%)";
    planYearly = true;
    planMonthly = false;

    allPlanPricesArray.forEach((planPrice, index) => {
      x += 1;
      const twoMonthsFreeText = document.createElement("span");
      twoMonthsFreeText.setAttribute("class", "two-months-free");
      x < 4 ? (twoMonthsFreeText.textContent = " 2 months free") : "none";

      planPrice.textContent = yearPlans[index];
      planInfoArray[index].appendChild(twoMonthsFreeText);
    });
  } else {
    x = 0;
    monthlySwitch.style.color = "hsl(213, 96%, 18%)";
    yearlySwitch.style.color = "hsl(231, 11%, 63%)";
    planYearly = false;
    planMonthly = true;

    allPlanPricesArray.forEach((planPrice, index) => {
      const existingTwoMonthsFreeText =
        planInfoArray[index].querySelector(".two-months-free");

      if (existingTwoMonthsFreeText) {
        planInfoArray[index].removeChild(existingTwoMonthsFreeText);
      }
      planPrice.textContent = monthPlans[index];
    });
  }
}

//Go back button-2
prevBtn2.addEventListener("click", (event) => {
  x = 0;
  renderNextOrPreviousPage(
    form2,
    form1,
    btns2,
    btns1,
    sideBarCircle2,
    sideBarCircle1
  );
});

//form -3

nextBtn2.addEventListener("click", (event) => {
  console.log(planMonthly);
  console.log(planYearly);
  renderNextOrPreviousPage(
    form2,
    form3,
    btns2,
    btns3,
    sideBarCircle2,
    sideBarCircle3
  );
  if (planMonthly) {
    // priceAddOns.textContent
    console.log("hello from plan Monthly");
    priceAddOns[0].textContent = "+$1/mo";
    priceAddOns[1].textContent = "+$2/mo";
    priceAddOns[2].textContent = "+$2/mo";
  }
  if (planYearly) {
    // priceAddOns.textContent
    console.log("hello from plan Yearly");
    priceAddOns[0].textContent = "+$10/yr";
    priceAddOns[1].textContent = "+$20/yr";
    priceAddOns[2].textContent = "+$20/yr";
  }
});

const priceAddOns = document.querySelectorAll(".price-add-ons");
const AddOns = document.querySelectorAll(".inputAddOns");

AddOns.forEach((checkbox) => {
  const addOnsBox = checkbox.closest(".add-ons-box");
  if (checkbox.checked) {
    addOnsBox.classList.add("purple-border");
  } else {
    addOnsBox.classList.remove("purple-border");
  }
  checkbox.addEventListener("change", function () {
    if (checkbox.checked) {
      addOnsBox.classList.add("purple-border");
    } else {
      addOnsBox.classList.remove("purple-border");
    }
  });
});

//Go back button-3
prevBtn3.addEventListener("click", (event) => {
  renderNextOrPreviousPage(
    form3,
    form2,
    btns3,
    btns2,
    sideBarCircle3,
    sideBarCircle2
  );
});

//form-4

nextBtn3.addEventListener("click", finishingUpPage);

function finishingUpPage(event) {
  event.preventDefault();
  renderNextOrPreviousPage(
    form3,
    form4,
    btns3,
    btns4,
    sideBarCircle3,
    sideBarCircle4
  );

  const planName = document.querySelector(".plan-name");
  const planAmount = document.querySelector(".plan-price-selected");
  const addOns = document.querySelector(".addons");
  const totalText = document.querySelector(".total-text");
  const totalPrice = document.querySelector(".total-price");
  let amount = 0;

  const plan = document.querySelector("input[type='radio']:checked");
  const planDecided = plan.value;
  const planDecidedAmount =
    plan.nextElementSibling.lastElementChild.lastElementChild.textContent;
  const planDecidedAmountYearly =
    plan.nextElementSibling.lastElementChild.lastElementChild
      .previousElementSibling.textContent;
  console.log(planDecided);
  console.log(planDecidedAmount);
  console.log(planDecidedAmountYearly);

  const addOnsServices = document.querySelectorAll(
    ".add-ons-box-first input[type='checkbox']:checked"
  );

  if (planMonthly) {
    planName.innerHTML = `<p class="plan-name">${planDecided}(Monthly)</p>`;
    planAmount.textContent = planDecidedAmount;
    console.log(typeof planDecidedAmount.slice(1, -3));

    amount = amount + Number(planDecidedAmount.slice(1, -3));
    console.log(amount);
    addOns.innerHTML = "";
    addOnsServices.forEach((addon) => {
      console.log("hrllll");
      const lastELementAmount =
        addon.parentElement.parentElement.lastElementChild.textContent;
      addOns.innerHTML += `<div class="selected-addon">
          <div class="service-name">${addon.value}</div>
          <div class="service-price">${lastELementAmount}</div>
          </div>`;
      console.log(lastELementAmount.slice(1, -3));
      amount = amount + Number(lastELementAmount.slice(2, -3));
      console.log(amount);
    });
    totalText.textContent = "Total (per month)";
    totalPrice.textContent = `+$${amount}/mo`;
  }
  if (planYearly) {
    planName.innerHTML = `<p class="plan-name">${planDecided}(Yearly)</p>`;
    planAmount.textContent = planDecidedAmountYearly;
    amount = amount + Number(planDecidedAmountYearly.slice(1, -3));
    addOns.innerHTML = "";
    addOnsServices.forEach((addon) => {
      const lastELementAmount =
        addon.parentElement.parentElement.lastElementChild.textContent;
      addOns.innerHTML += `<div class="selected-addon">
        <div class="service-name">${addon.value}</div>
        <div class="service-price">${lastELementAmount}</div>
        </div>`;
      amount = amount + Number(lastELementAmount.slice(2, -3));
    });
    totalText.textContent = "Total (per year)";
    totalPrice.textContent = `+$${amount}/yr`;
  }
}

const changePlan = document.querySelector(".plan-name-selected a");
console.log(changePlan);
changePlan.addEventListener("click", (e) => {
  renderNextOrPreviousPage(
    form4,
    form2,
    btns4,
    btns2,
    sideBarCircle4,
    sideBarCircle2
  );
});

prevBtn4.addEventListener("click", (event) => {
  renderNextOrPreviousPage(
    form4,
    form3,
    btns4,
    btns3,
    sideBarCircle4,
    sideBarCircle3
  );
});

nextBtn4.addEventListener("click", (e) => {
  form4.style.display = "none";
  form5.style.display = "flex";
  btns4.style.display = "none";
});

// });
